package com.choinisae.nisaechoi.changewallpaperimage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.choinisae.nisaechoi.changewallpaperimage.service.ForegroundService;

/**
 * Created by donghaechoi on 2016. 9. 10..
 */
public class BootOnReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Intent serviceIntent = new Intent(context, ForegroundService.class);
            context.startService(serviceIntent);
        }
    }
}
