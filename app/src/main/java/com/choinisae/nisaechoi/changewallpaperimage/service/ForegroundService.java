package com.choinisae.nisaechoi.changewallpaperimage.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;

import com.choinisae.nisaechoi.changewallpaperimage.MainActivity;
import com.choinisae.nisaechoi.changewallpaperimage.R;
import com.choinisae.nisaechoi.changewallpaperimage.receiver.ScreenOffReceiver;

/**
 * Created by donghaechoi on 2016. 7. 5..
 */
public class ForegroundService extends Service {

    private static final String TAG = ForegroundService.class.getSimpleName();
    private ScreenOffReceiver mReceiver;

    // com.choinisae.nisaechoi.changewallpaperimage
    // 광고 단위 아이디 : ca-app-pub-2825752146789052/2740193127

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        mReceiver = new ScreenOffReceiver();
        registerReceiver(mReceiver, filter);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle(getString(R.string.app_name));
        notificationBuilder.setContentText(getString(R.string.app_name) + "이 동작중입니다.");
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        notificationBuilder.setLargeIcon(bitmap);

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        notificationBuilder.setContentIntent(pendingIntent);

        startForeground(1, notificationBuilder.build());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
