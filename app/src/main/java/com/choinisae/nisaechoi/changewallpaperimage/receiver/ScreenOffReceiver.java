package com.choinisae.nisaechoi.changewallpaperimage.receiver;

import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;

import com.choinisae.nisaechoi.changewallpaperimage.facade.ImageFacade;

import java.io.IOException;
import java.util.Random;

/**
 * Created by donghaechoi on 2016. 7. 5..
 */
public class ScreenOffReceiver extends BroadcastReceiver {

    private WallpaperManager mWallpaperManager;
    public static int sPosition = 0;
    public static final String TAG = ScreenOffReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            mWallpaperManager = WallpaperManager.getInstance(context);
            ImageFacade facade = new ImageFacade(context);
            final Cursor cursor = facade.queryAllpaths();

            while (true) {
                int random = new Random().nextInt(cursor.getCount()) + 1;
                if (sPosition != random) {
                    cursor.move(random);
                    sPosition = random;
                    break;
                }
            }
            final String picturePath = cursor.getString(1);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        mWallpaperManager.setBitmap(BitmapFactory.decodeFile(picturePath));
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        cursor.close();
                    }
                }
            });
        }
    }
}
