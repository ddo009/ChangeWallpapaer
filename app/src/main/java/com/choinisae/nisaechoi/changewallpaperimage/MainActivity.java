package com.choinisae.nisaechoi.changewallpaperimage;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.WallpaperManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.choinisae.nisaechoi.changewallpaperimage.adapter.recyclerview.MyRecyclerAdapter;
import com.choinisae.nisaechoi.changewallpaperimage.facade.ImageFacade;
import com.choinisae.nisaechoi.changewallpaperimage.service.ForegroundService;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MyRecyclerAdapter.OnItemClickListener {


    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int GALLERY_PICK_REQUEST_CODE = 3000;
    private static final int IMAGE_CROP_REQUEST_CODE = 1000;

    private boolean isServiceRunning;
    private ImageFacade mFacade;
    private MyRecyclerAdapter mGridRecyclerAdapter;
    private WallpaperManager mWallpaperManager;
    private String mFilePath;
    private int mDiviceWidth;
    private int mDiviceHeight;
    private Button mServiceButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWallpaperManager = WallpaperManager.getInstance(this);

        mFacade = new ImageFacade(getApplicationContext());

        initAds();
        checkDeviceScreenSize();
        recyclerViewSettings(mFacade.queryAllpaths());

        findViewById(R.id.add_btn).setOnClickListener(this);
        mServiceButton = (Button) findViewById(R.id.service_btn);
        mServiceButton.setOnClickListener(this);

        isServiceRunning = isServiceRunningCheck();
        if (isServiceRunning) {
            mServiceButton.setText("STOP SERVICE");
        } else {
            mServiceButton.setText("START SERVICE");
        }

    }

    // 서비스 활성 여부 체크 Method
    public boolean isServiceRunningCheck() {
        ActivityManager manager = (ActivityManager) this.getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.choinisae.nisaechoi.changewallpaperimage.service.ForegroundService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        switch (view.getId()) {
            // addButton 클릭시
            case R.id.add_btn:
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALLERY_PICK_REQUEST_CODE);
                break;
            // completeButton 클릭시
            case R.id.service_btn:
                if (isServiceRunning == false) {
                    final Cursor cursor = mFacade.queryAllpaths();

                    double random = Math.random() * cursor.getCount() + 1;
                    cursor.move((int) random);

                    // 옵션설정
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;

                    float widthScale = options.outWidth / mDiviceWidth;
                    float heightScale = options.outHeight / mDiviceHeight;
                    float scale = widthScale > heightScale ? widthScale : heightScale;

                    // 스케일에 따라 inSampleSize 재정의
                    if (scale >= 8) {
                        options.inSampleSize = 8;
                    } else if (scale >= 6) {
                        options.inSampleSize = 6;
                    } else if (scale >= 4) {
                        options.inSampleSize = 4;
                    }

                    if (cursor.getCount() != 0) {
                        final Bitmap bitmap = BitmapFactory.decodeFile(cursor.getString(1), options);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    mWallpaperManager.setBitmap(bitmap);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        Toast.makeText(MainActivity.this, "사진을 변경했습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "사진을 불러오지 못했습니다.", Toast.LENGTH_SHORT).show();
                    }
                    cursor.close();
                    startService(serviceIntent);
                    mServiceButton.setText("Stop Service");
                    isServiceRunning = true;
                } else {
                    stopService(serviceIntent);
                    mServiceButton.setText("Start Service");
                    isServiceRunning = false;
                    Toast.makeText(MainActivity.this, "서비스를 종료했습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String mmCropFile;

        if (requestCode == GALLERY_PICK_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            Uri selectImage = data.getData();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            // 암묵적 인텐트
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(selectImage, "image/*");
            cropIntent.putExtra("crop", "true");
            // x축과 y축의 비율이 9:16
            cropIntent.putExtra("aspectX", 9);
            cropIntent.putExtra("aspectY", 16);
            // 리턴 데이터를 받지 않고 파일로 저장
            cropIntent.putExtra("return-data", false);
            // 저장될 파일명
            mmCropFile = System.currentTimeMillis() + ".jpg";
            // jpg로 저장
            cropIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
            // 새로운 파일 생성
            File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/randomWallPaper/", mmCropFile);
            // 생성된 파일의 path얻기
            mFilePath = file.getPath();
            // 파일 저장
            Uri uri = Uri.fromFile(file);
            cropIntent.putExtra("output", uri);
            // 크롭 시작
            startActivityForResult(cropIntent, IMAGE_CROP_REQUEST_CODE);
            // 크롭후 받아오는 곳
        } else if (requestCode == IMAGE_CROP_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            mFacade.insertPath(mFilePath);
            mGridRecyclerAdapter.swapCursor(mFacade.queryAllpaths());
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onItemLongClick(View view, int position) {
        Cursor cursor = mFacade.queryAllpaths();
        cursor.moveToPosition(position);
        int deleted = mFacade.deleteImage("_id=" + cursor.getLong(0), null);
        if (deleted > 0) {
            Toast.makeText(getApplicationContext(), "삭제 되었습니다", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onItemLongClick: " + cursor.getPosition());
            mGridRecyclerAdapter.swapCursor(mFacade.queryAllpaths());
        } else {
            Toast.makeText(MainActivity.this, "삭제 실패", Toast.LENGTH_SHORT).show();
        }
        cursor.close();
    }

    public void checkDeviceScreenSize() {
        // 디바이스 사이즈 확인
        DisplayMetrics displaySize = getApplicationContext().getResources().getDisplayMetrics();
        mDiviceWidth = displaySize.widthPixels;
        mDiviceHeight = displaySize.heightPixels;
    }


    /**
     * RecyclerView Settings
     *
     * @param cursor = mFacade.queryAllpaths();
     */
    public void recyclerViewSettings(Cursor cursor) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mGridRecyclerAdapter = new MyRecyclerAdapter(cursor);

        DefaultItemAnimator animator = new DefaultItemAnimator();
        animator.setChangeDuration(1000);
        animator.setAddDuration(1000);
        animator.setRemoveDuration(1000);

        mGridRecyclerAdapter.setOnItemClickListener(this);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 4);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mGridRecyclerAdapter);
        recyclerView.setItemAnimator(animator);
    }

    public void initAds() {
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }


}
