package com.choinisae.nisaechoi.changewallpaperimage;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;


public class IntroActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 0;
    static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_NETWORK_STATE};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        PermissionsChecker permissionsChecker = new PermissionsChecker(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android 6.0 권한 체크
            if (permissionsChecker.lacksPermissions(PERMISSIONS)) {
                startPermissionsActivity();
            } else {
                startMainActivity();
            }
        } else {
            startMainActivity();
        }
    }


    private void startPermissionsActivity() {
        PermissionsActivity.startActivityForResult(this, REQUEST_CODE, PERMISSIONS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == PermissionsActivity.PERMISSIONS_DENIED) {
            finish();
        } else {
            startMainActivity();
        }
    }

    public void startMainActivity() {
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                File randomWallPaper = new File(Environment.getExternalStorageDirectory() + "/randomWallPaper/");
                File nomedia = new File(Environment.getExternalStorageDirectory() + "/randomWallPaper/.nomedia");
                if (!randomWallPaper.exists()) {
                    randomWallPaper.mkdirs();
                    try {
                        nomedia.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                startActivity(new Intent(IntroActivity.this, MainActivity.class));
                finish();
            }
        }, 500);
    }
}
